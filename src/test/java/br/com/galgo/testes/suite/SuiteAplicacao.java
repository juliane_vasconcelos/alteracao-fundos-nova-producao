package br.com.galgo.testes.suite;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.com.galgo.alterar.portal.AlterarFundosPortal;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.suite.StopOnFirstFailureSuite;
import br.com.galgo.testes.recursos_comuns.utils.SuiteUtils;

@RunWith(StopOnFirstFailureSuite.class)
@Suite.SuiteClasses({ AlterarFundosPortal.class })
public class SuiteAplicacao {

	private static final String PASTA_SUITE = "Aplicação";

	@BeforeClass
	public static void setUp() throws Exception {
		SuiteUtils.configurarSuiteDefault(Ambiente.PRODUCAO_NEW, PASTA_SUITE);
	}

}
